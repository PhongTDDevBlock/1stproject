curl --location --request POST 'https://outlook.office.com/webhook/5ab5467e-1797-4d0b-aa4f-b3b839712df8@6b4bc897-d7e8-4d36-9d79-3fb5f24b50ad/IncomingWebhook/81f16f1e7d714b4bad35e882f44f19cd/e8489ab3-92db-4be2-bec3-4bcd28e5ad1e' \
--header 'Content-Type: application/json' \
--data-raw '{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": "CC4A31",
    "summary": "Alarm",
    "sections": [
        {
            "@type": "TextBlock",
            "text": "**Build failll**",
            "facts": [
                {
                    "name": "Project",
                    "value": "Value 1"
                },
                {
                    "name": "Status",
                    "value": "Value 2"
                },
                {
                    "name": "Status",
                    "value": "Value 2"
                }
            ]
        }
    ],
    "potentialAction": [
        {
            "@type": "OpenUri",
            "name": "Click",
            "targets": [
                {
                    "os": "default",
                    "uri": "https://google.com"
                }
            ]
        }
    ]
}'