#/bin/bash

#declare variable
notifyStatus=$1
ProjectName=$2
BuildID=$3

#check empty notyfy statys
if [[ -z "$notifyStatus" ]]; then
  echo -e "\n====> ERROR: notifyStatus can not be empty\n"
  exit 1
fi

#check category for alarm
if [ $notifyStatus == 1 ];
  then
    titlealarm="Build Fail"
    themeColor="CC4A31"
  else
    titlealarm="Build Suscees..."
    themeColor="00FE0D"
fi

#import link webhook target
importlink()
{
cat <<EOF
$linkcurlmicrosoftteams
EOF
}

#import message card into rawdata
generate_post_data()
{
    cat <<EOF
{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": '$themeColor',
    "summary": "Alarm From Project : $ProjectName",
    "sections": [
        {
            "@type": "TextBlock",
            "text": "**$titlealarm**",
            "facts": [
                {
                    "name": "Project",
                    "value": '$ProjectName'
                },
                {
                    "name": "Status",
                    "value": '$notifyStatus'
                }
            ]
        },
        {
            "@type": "Image",
            "url": "https://devblock.net/images/services/service.png"
        }
    ],
    "potentialAction": [
        {
            "@type": "OpenUri",
            "name": "Click Here For More Detail",
            "targets": [
                {
                    "os": "default",
                    "uri": "https://bitbucket.org/$ProjectName/addon/pipelines/home#!/results/$BuildID"
                }
            ]
        }
    ]
}
EOF
}

curl --location --request POST "$(importlink)" \
--header 'Content-Type: application/json' \
--data-raw "$(generate_post_data)"
