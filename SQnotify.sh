#/bin/bash

#declare variable
notifyStatus=$1
ProjectName=$2


CodeSmell=$(curl --location --request GET 'https://sonarqube.devblock.io/api/issues/search?componentKeys='$ProjectName'&types=CODE_SMELL' | jq .issues | jq length)
Bugs=$(curl --location --request GET 'https://sonarqube.devblock.io/api/issues/search?componentKeys='$ProjectName'&types=BUG' | jq .issues | jq length)
Vuln=$(curl --location --request GET 'https://sonarqube.devblock.io/api/issues/search?componentKeys='$ProjectName'&types=VULNERABILITY' | jq .issues | jq length)
SECURITY_HOSTSPOT=$(curl --location --request GET 'https://sonarqube.devblock.io/api/issues/search?componentKeys='$ProjectName'&types=SECURITY_HOTSPOT' | jq .issues | jq length)

#check empty notyfy statys
if [[ -z "$notifyStatus" ]]; then
  echo -e "\n====> ERROR: notifyStatus can not be empty\n"
  exit 1
fi

#check category for alarm
if [ $notifyStatus == 1 ];
  then
    titlealarm="Build Fail"
    themeColor="CC4A31"
  else
    titlealarm="Build Suscess..."
    themeColor="00FE0D"
fi

#import link webhook target
importlink()
{
cat <<EOF
$linkcurlmicrosoftteams
EOF
}
echo $linkcurlmicrosoftteams
#import message card into rawdata
generate_post_data()
{
    cat <<EOF
{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": '$themeColor',
    "summary": "Alarm From Project : $ProjectName",
    "sections": [
        {
            "@type": "TextBlock",
            "text": "**$titlealarm** $ProjectName",
            "facts": [
                {
                    "name": "Bugs:",
                    "value": '$Bugs'
                },
                {
                    "name": "CodeSmell:",
                    "value": '$CodeSmell'
                },
                {
                    "name": "Vuln:",
                    "value": '$Vuln'
                },
                {
                    "name": "SECURITY_HOSTSPOT:",
                    "value": '$SECURITY_HOSTSPOT'
                }
            ]
        }
    ],
    "potentialAction": [
        {
            "@type": "OpenUri",
            "name": "Click Here For More Detail",
            "targets": [
                {
                    "os": "default",
                    "uri": "http://sonarqube.devblock.io/dashboard?id=$ProjectName"
                }
            ]
        }
    ]
}
EOF
}

curl --location --request POST "$(importlink)" \
--header 'Content-Type: application/json' \
--data-raw "$(generate_post_data)"
