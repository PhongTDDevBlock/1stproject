#/bin/bash

notifyStatus=$1
linkcurlmicrosoftteams=$2

if [[ -z "$notifyStatus" ]]; then
  echo -e "\n====> ERROR: notifyStatus can not be empty\n"
  exit 1
fi

if [ $notifyStatus == 1 ];
  then
    titlealarm="fail"
    themeColor="CC4A31"
    echo $titlealarm
    echo $themeColor
  else
    titlealarm="suscees"
    themeColor="00FE0D"
    echo $titlealarm
    echo $themeColor
fi

echo $linkcurlmicrosoftteams